package com.ruoyi.web.api;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 对外API接口请求处理
 *
 * @author lilp
 */
@RestController
@RequestMapping("/api/user")
public class ApiUserController {

    @Autowired
    private ISysUserService userService;

    /**
     * 获取用户信息
     */
    @GetMapping(value = "/{userId}")
    public AjaxResult getById(@PathVariable(value = "userId") Long userId) {
        return AjaxResult.success(userService.selectUserById(userId));
    }
}
